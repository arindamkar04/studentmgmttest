@test
Feature: Testing StudentmgmtApplication REST API
    User should be able to Add, Update, Fetch and delete student record
    User should get appropriate error messages when invalid scenarios are encountered

    Scenario Outline: Validate add new Student with valid inputs and then delete the same student
        Given user adds new student with "<studentId>" "<firstName>" "<lastName>" "<class>" "<nationality>"
        Then user should do successful student data search with "<studentId>"
        Then user should delete student data with "<studentId>"
        Then user should do unsuccessful student data search with "<studentId>"

        Examples:
        |studentId | firstName | lastName | class | nationality |
        | 110001   | Tony      | Stark    | 1A    | American    |

