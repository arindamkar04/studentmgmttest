package form;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Arindam Kar on 21/10/2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentDataForm {
    private Long id;
    private String firstName;
    private String lastName;
    private String studentClass;
    private String nationality;

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id +
                ", \"firstName\":\"" + firstName + "\"" +
                ", \"lastName\":\"" + lastName + "\"" +
                ", \"studentClass\":\"" + studentClass + "\"" +
                ", \"nationality\":\"" + nationality + "\"" +
                '}';
    }
}
