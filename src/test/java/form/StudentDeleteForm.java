package form;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Arindam Kar on 21/10/2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentDeleteForm {

    private Long id;

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id +
                '}';
    }
}
