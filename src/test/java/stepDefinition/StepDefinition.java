package stepDefinition;


import form.StudentDataForm;
import form.StudentDeleteForm;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;


public class StepDefinition {

    private static final String BASE_URL = "http://localhost:9080/studentmgmt";
    private static Response response;

    @When("user adds new student with {string} {string} {string} {string} {string}")
    public void user_adds_new_student_with(String studentId, String firstName, String lastName, String studentClass, String nationality) {

        RestAssured.baseURI = BASE_URL;
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        StudentDataForm form = new StudentDataForm(Long.parseLong(studentId), firstName, lastName, studentClass, nationality);
        response = request.body(form.toString())
                .post("/addStudent");
        Assert.assertEquals(200, response.statusCode());
        Assert.assertTrue(response.asString().contains(studentId));
    }

    @Then("user should do successful student data search with {string}")
    public void user_should_do_successful_student_data_search_with(String studentId) {
        RestAssured.baseURI = BASE_URL;
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        response = request.get("/fetchStudents?id=" + studentId);
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertTrue(response.asString().contains(studentId));
    }

    @Then("user should do unsuccessful student data search with {string}")
    public void user_should_do_unsuccessful_student_data_search_with(String studentId) {
        RestAssured.baseURI = BASE_URL;
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        response = request.get("/fetchStudents?id=" + studentId);
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertFalse(response.asString().contains(studentId));
    }

    @Then("user should delete student data with {string}")
    public void user_should_delete_student_data_with(String studentId) {

        RestAssured.baseURI = BASE_URL;
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        StudentDeleteForm form = new StudentDeleteForm(Long.parseLong(studentId));
        response = request.body(form.toString())
                .delete("/deleteStudent");
        Assert.assertEquals(response.statusCode(), 200);
    }

}
