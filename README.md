# README #

Readme for Test repo of Student Management Application

### Build app locally

1. Make sure maven is installed in target machine and MAVEN_HOME is set

2. clone the repo

3. Open terminal and browse to project root folder

4. type mvn test - to start the test

## **Full Project Documentation with screenshots and How Tos
https://docs.google.com/document/d/1BLxnYOGFg7uuDeReJT2j0QYgaZHX-a0EGhgntrkrM4s/edit?usp=sharing

